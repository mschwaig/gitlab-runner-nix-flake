To build this runner without activating the configuration you can run

```
nix build .#nixosConfigurations.nixos-gitlab-runner.config.system.build.toplevel
```
(assuming you provided a dummy registration token for the runner)

To activate this configuration you can run

```
nixos-rebuild switch --flake .nixos-gitlab-runner
```

since this turns the system you run it on into the one described by the configuration in this repo you would probably have to adapt the hardware-specific parts of it a bit and/or try to run it in a container first.

If you set the required post-commit hook in the config repo locally on the runner, you can remotely update it from a github repo via my other project [nixos-rebuild-by-post-receive-hook](https://gitlab.com/mschwaig/nixos-rebuild-by-post-receive-hook).
