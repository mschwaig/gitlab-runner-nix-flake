{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.nixos-rebuild-by-post-receive-hook.url = "gitlab:mschwaig/nixos-rebuild-by-post-receive-hook";

  outputs = { self, nixpkgs, nixos-rebuild-by-post-receive-hook }: {

    nixosConfigurations.nixos-gitlab-runner = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules =
        [
          ./hardware-configuration.nix
          ./networking.nix # generated at runtime by nixos-infect

          ./gitlab-runner-service.nix

          ({ pkgs, ... }: {

            # add language features that enable flakes
            nix = {
              package = pkgs.nixFlakes;
              extraOptions = ''
                experimental-features = nix-command flakes
              '';
            };

            # add system packages
            environment.systemPackages = with pkgs; [
              git (neovim.override { vimAlias = true; })

              nixos-rebuild-by-post-receive-hook.defaultPackage.x86_64-linux
            ];
            programs.vim.defaultEditor = true;

            # Let 'nixos-version --json' know about the Git revision
            # of this flake.
            system.configurationRevision = nixpkgs.lib.mkIf (self ? rev) self.rev;

            boot.cleanTmpDir = true;
            networking.hostName = "nixos-gitlab-runner";
            networking.firewall.allowPing = true;
            services.openssh.enable = true;  users.users.root.openssh.authorizedKeys.keys = [
              "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHNdCt+2TSagVo60uRwVcmqpnw4dmObs1v8texBvAoCR mschwaig@mutalisk"
              "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC4ywOBYF0d/7/3Qyz1+iFOMa3oQsE2/2WbqZWoez2p+CMiVlVcPM3GdROPyg6Q6ovp00F36bIm3id9A+O3LJOsznGe4TdvgqtqqBpZCobuTrfehEMcTn7/ykkMrCH2L07QQCTnJtfuL3ww6V0YSPL4FLwn/OViSfd/QyLDCLEHBaBTJUOXId8NWPjNi7G4JHwodSa8cOxd9yUN/fvIOtPIEs6kAKFrg+wds/GG+xqKfCx8tGy0Dq1huU3hTCCCbGVCkiMQGa+J7SeIw9ioL1Xavs0q8+N2vVw8ipoGGOWKXcDOm8/41aZ99fVAJOVQy9gIPQhIWDHC6S/vbDC4QzQtTsvn+tcxW7nYPoCw1k5mMUU9Dn6tdjE5zUE3sZbQ8BqpubFa+VsxDJU5EHTk9ihtqrf2bAFhE/7tyE91qxUWAAn3nIUQtCQjKa04kHkC8KKl43fcrC6K1ZZpWow82dJ7UkjnC9/kQfLiEDiLWZNDCfDpOorTpgAivhTNVn2VZGIkM3EslpPkYORjKNlnjymJ2mRla/bEg3l2L3ToM80khm5LBwrxpPFq6M0r6GnOcifVnlzdFYy0qSwO3s8QJJ/fJKJm+P6hq/3r9/vGUfxicPTS9s6a1ob3T/kdelEzSE0nNpnZB5RpGuhvTV9oNxFevPWSalsj7q8gN/Dx5iXa+Q== git@gitlab.com"
            ];
          })
        ];
      };
  };
}
