{ lib, ... }: {
  # This file was populated at runtime with the networking
  # details gathered from the active system.
  networking = {
    nameservers = [ "8.8.8.8" ];
    defaultGateway = "46.101.128.1";
    defaultGateway6 = "";
    dhcpcd.enable = false;
    usePredictableInterfaceNames = lib.mkForce true;
    interfaces = {
      eth0 = {
        ipv4.addresses = [
          { address="46.101.141.34"; prefixLength=18; }
{ address="10.19.0.5"; prefixLength=16; }
        ];
        ipv6.addresses = [
          { address="fe80::b427:27ff:feb0:be56"; prefixLength=64; }
        ];
        ipv4.routes = [ { address = "46.101.128.1"; prefixLength = 32; } ];
        ipv6.routes = [ { address = ""; prefixLength = 32; } ];
      };
    };
  };
  services.udev.extraRules = ''
    ATTR{address}=="b6:27:27:b0:be:56", NAME="eth0"
    ATTR{address}=="2a:03:88:73:cf:51", NAME="eth1"
  '';
}
